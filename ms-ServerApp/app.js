var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var session = require('express-session');

const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./api/controllers/swagger-api-docs/swagger.json');
var swaggerOptions = {
  customCss: `
    .swagger-ui .topbar { display: none }
    .nostyle { font-size: 16px }
  `
};

const envVar = require('./api/controllers/utils/env');

var indexRouter = require('./routes/index');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session({secret: envVar.sessionKey, resave: false, saveUninitialized: true}));
app.use(express.static(path.join(__dirname, 'public')));

// CORS
app.use(function (req, res, next) {
  /* res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next(); */

  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', '*');

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);

  // Pass to next layer of middleware
  next();
});

// Timestamp for Logs
app.use((req,res,next)=>{
    var currentDate = new Date();
      
    var getDateAndTime = `Date: ${currentDate.getDate()}/${currentDate.getMonth()+1}/${currentDate.getFullYear()} Time: ${currentDate.getHours()}:${currentDate.getMinutes()}:${currentDate.getSeconds()}`

    console.log('TimeStamp: ',getDateAndTime);
    next();
});

// Swagger 
app.use('/api/blockchain/ms-api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument,swaggerOptions));

app.use('/', indexRouter);

//Import Routes

var routesApi = require('./api/routes/routes');
app.use('/api', routesApi);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
