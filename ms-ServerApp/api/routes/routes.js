const express = require('express');
const app = express();
const router = express.Router();
var multer = require('multer');

var jwtTokenAuth = require('../middleware/jwt-protect-routes/jwt-protect-routes');

// Handling File Upload for Login Page: Starts
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'public/certificate');
  },
  filename: function (req, file, cb) {
    cb(null, 'user_cert.txt');
  }
});

var upload = multer({ storage: storage });
// Handling File Upload for Login Page: Ends

// Handling File Upload for Uploading CSV: Starts
var storageDataFile = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'public/dataFile');
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  }
});

var uploadDataFile = multer({ storage: storageDataFile });
// Handling File Upload for Uploading CSV: Ends


// Register Org/Signup
var ctlrOrgRegister = require('../blockchain/controllers/registerOrganization/registerOrganization')
router.post('/blockchain/register-organization', ctlrOrgRegister.registerOrgs);

// Save Certificate
var ctrlSaveCert = require('../blockchain/controllers/get-certificate-from-hash/get-certificate');
router.post('/blockchain/get-certificate-from-hash', ctrlSaveCert.getCertFromHash);

// Validate Certificate/Login step 1
var ctrlOrgLogin = require('../blockchain/controllers/organization-login/organization-login');
router.post('/blockchain/get-org-details', upload.single('userCertificate'), ctrlOrgLogin.validateCertificate);

// Validate Token/Login step 2
var ctrlValidateToken = require('../blockchain/controllers/validate-token/validate-token');
router.post('/blockchain/validate-token', ctrlValidateToken.validateToken);

// Get Org details
var ctrlGetOrgDetails = require('../blockchain/controllers/get-org-details/get-org-details');
router.get('/blockchain/get-org-sepcific-details', jwtTokenAuth, ctrlGetOrgDetails.queryOrgDetails);

// Upload CSV file
var ctrlUploadDataFile = require('../blockchain/controllers/upload-data-file/bc-upload-data-file');
router.post('/blockchain/upload-data-file', jwtTokenAuth, uploadDataFile.single('dataFile'), ctrlUploadDataFile.uploadDataFile);

//Get Data File From IPFS
var ctrlGetUpliadedDataFile = require('../blockchain/controllers/upload-data-file/bc-get-data-file');
router.get('/blockchain/get-uploaded-data-file', jwtTokenAuth, ctrlGetUpliadedDataFile.queryUploadedDataFile);

// Update Organization Details
var ctrlUpdateOrgDetails = require('../blockchain/controllers/update-organization-details/update-organization-details');
router.post('/blockchain/update-organization-profile', jwtTokenAuth, ctrlUpdateOrgDetails.updateOrganizationDetails)


// Revoke Organization Access
var ctrlRevokeOrhAccess = require('../blockchain/controllers/revoke-org-access/revoke-org-access');
router.post('/blockchain/revoke-organization-access',ctrlRevokeOrhAccess.revokeOrganizationAccess);

// Saving Data to BigChain : Parsed Data
var ctrlSaveParsedData = require('../blockchain/controllers/bigChainDB/save-parsed-data/save-parsed-data');
router.post('/blockchain/save-parsed-data', jwtTokenAuth, ctrlSaveParsedData.saveParsedData);


var ctrlSavebtnClick = require('../blockchain/controllers/save-csv-data/save-csv-data');
router.post('/blockchain/save-modified-data', ctrlSavebtnClick.addDataToIpfs);

var ctrlSaveModifiedData = require('../blockchain/controllers/save-csv-data/bc-save-csv-data');
router.post('/blockchain/save-modified-data-bc', jwtTokenAuth, ctrlSaveModifiedData.saveModifiedData);

var ctrlGetModifiedData = require('../blockchain/controllers/save-csv-data/getModifiedData');
router.get('/blockchain/get-modified-data-bc', jwtTokenAuth, ctrlGetModifiedData.getModifiedData);

var ctrlGetDataForWS = require('../blockchain/controllers/save-csv-data/get-data-for-ws');
router.get('/blockchain/get-data-for-ws', jwtTokenAuth, ctrlGetDataForWS.getDataForWS);

/* Sample */

var ctrlUtils = require('../blockchain/controllers/upload-data-file/testAsyncAwait');
router.get('/testApi', ctrlUtils.filterUsingLodash);

module.exports = router;