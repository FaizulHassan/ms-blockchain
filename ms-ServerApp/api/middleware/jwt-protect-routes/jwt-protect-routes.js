const express = require('express');
const app = express();
const jwt = require('jsonwebtoken');
const envVar = require('../../controllers/utils/env');

module.exports = (req, res, next) => {
    try {
        const token = req.headers.authorization.split(' ')[1];
        const decoded = jwt.verify(
            token,
            envVar.jwtSecret
        );

        console.log('from JWT req.session::', req.session);
        console.log('from JWT req.session.userEmail::', req.session.userEmail);

        /* if (req.session.userEmail != decoded._orgMailId) {
            res.status(401).json({
                message: 'Token mismatch ! Enter a valid token'
            });
        } else {
            console.log('JWT Token:', decoded);

            res.locals.orgDetails = {
                _orgMailId: decoded._orgMailId,
                _ipfsHash: decoded._ipfsHash
            }
            next();
        } */
        console.log('JWT Token:', decoded);

        res.locals.orgDetails = {
            _orgMailId: decoded._orgMailId,
            _ipfsHash: decoded._ipfsHash
        }
        next();
    } catch (err) {
        return res.status(401).json({
            message: 'unauthorized access',
            error: err
        });
    }
};
