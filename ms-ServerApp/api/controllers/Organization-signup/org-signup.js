//This is just a sample

const express = require('express');
const app = express();

const jwt = require('jsonwebtoken');

const emailToken = require('../email-config/email-token');

exports.organizationSignup = (req, res) => {
    const companyName = req.body.companyName;
    const companyUrl = req.body.companyUrl;
    const companyMailId = req.body.companyMailId

    var orgToken = jwt.sign(
        {
            _companyName: companyName,
            _companyUrl: companyUrl,
            _companyMailId: companyMailId
        },
        'jwt_Secret_Key_for_Moolah_Sense_Of_32Bit_String',
        {
            expiresIn: '10950d'
        }
    );

    const tokenArg = {
        toEmail: companyMailId,
        token: orgToken,
        companyName: companyName
    }
    
    console.log(tokenArg);

    if(orgToken){
        console.log('Token Generated');
        emailToken.emailToken(tokenArg);
        console.log('Mail Sent Successful');
    }

    return res.status(200).json({
        message: 'sign up successful ! you will receive an email shortly'        
    });
};
