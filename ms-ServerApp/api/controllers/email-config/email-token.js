const nodemailer = require('nodemailer');
const ejs = require('ejs');
var fs = require('fs');

var template = fs.readFileSync(__dirname+'/email.ejs',{encoding:'utf-8'});

// Generate test SMTP service account from ethereal.email
// Only needed if you don't have a real mail account for testing
exports.emailToken = account => {
    console.log('Email:', account.toEmail);
    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
            user: 'faizul.m@zencode.guru', // generated ethereal user
            pass: 'P@ssword@123' // generated ethereal password
        }
    });

    // setup email data with unicode symbols
    let mailOptions = {
        from: '"MoolahSense Active Intelligence (M.A.I.) Blockchain" <moolahsense@gmail.com>', // sender address
        to: account.toEmail, // list of receivers
        subject: 'MoolahSense Active Intelligence: BC Token', // Subject line
        text: account.token, // plain text body
        html: ejs.render(fs.readFileSync(__dirname+'/email.ejs',{encoding:'utf-8'}), {token: account.token})
                

        // html body
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }
        console.log('Message sent: %s', info.messageId);
        // Preview only available when sending through an Ethereal account
        // console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

        // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
        // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
    });
};
