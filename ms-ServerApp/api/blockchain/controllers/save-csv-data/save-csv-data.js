var fs = require('fs');
var mkdirp = require('mkdirp');
var Json2csvParser = require('json2csv').Parser;
var ipfsAPI = require('ipfs-api');

async function addDataToIpfs(req, res) {
   var jsonObject = JSON.parse(req.body.jsonObject);
   var parentFileName = req.body.parentFileName;

   //const orgTokenDetails = res.locals.orgDetails;

   var csvFields = [req.body.csvFields];
   var csvData = '';
   var json2csvParser = '';
   var date = '';
   var currentDate = '';
   var modifiedFileName = '';

   /* Create a directory to save user data file */
   var userFilePath = `public/csv_dataFiles/users/faizul/save`
   mkdirp(userFilePath, function (err) {
      if (err) console.error(err)
      else {
         console.log('done creating directory');

         console.log('csvFields::', typeof (csvFields));
         console.log('csvFields::', (csvFields));

         json2csvParser = new Json2csvParser({ csvFields });

         console.log('type of jsonObject::', typeof (jsonObject));
         console.log('jsonObject::', jsonObject);

         try {
            csvData = json2csvParser.parse(jsonObject);
            console.log(csvData);
         }
         catch (error) {
            console.log('CSV parsing error');
         }

         date = new Date();
         currentDate = date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear();
         modifiedFileName = `save_${parentFileName}_${currentDate}.csv`

         fs.writeFile(`${userFilePath}/${modifiedFileName}`, csvData, (err) => {
            if (err) {
               console.log('Error in writing file to the user directory');
            } else {

               var hostName = req.headers.host;


               if (hostName == 'localhost:3300') {
                  fileEndpoint = `http://localhost:3300/${userFilePath.split('public/')[1]}/${modifiedFileName}`;
               } else {
                  fileEndpoint = `https://bc.askmai.io/${userFilePath.split('public/')[1]}/${modifiedFileName}`;

               }

               console.log('fileEndpoint path::', fileEndpoint);
               console.log('done writing file to user directory');

               console.log('console.log 2');
               let dataFileContent = new Buffer(csvData);

               function pushToArray(arr, obj) {
                  const index = arr.findIndex((e) => e.parentFile === obj.parentFile);
               
                  if (index === -1) {
                     arr.push(obj);
                     console.log('if');
                  } else {
                     arr[index]['children'].push(obj.children[0]);
                     console.log('index::', index);
                     console.log('else');
                  }
               }

               //Connecting to the ipfs network via infura gateway
               const ipfs = ipfsAPI('13.250.116.159', '5001', { protocol: 'http' });
               ipfs.files.add(dataFileContent, (err, hash) => {
                  if (err) {
                     console.log(err);
                  }
                  console.log('ipfs hash: ', hash);

                  res.status(200).json({
                     message: 'Upload Sucessful !',
                     ipfsHash: hash[0].hash,                     
                     fileEndpoint: fileEndpoint
                  })
               });


            }
         });
      }
   });
}

exports.addDataToIpfs = addDataToIpfs;