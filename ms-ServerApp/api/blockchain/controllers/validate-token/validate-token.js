const jwt = require('jsonwebtoken');


module.exports = {
    validateToken: (req, res) => {    
        try {
            var token = req.body.token;
            const decoded = jwt.verify(token, 'jwt_Secret_Key_for_Moolah_Sense_Of_32Bit_String');     
            console.log('decoded token:', decoded);
            
            res.status(200).json({
                message: 'authentication successful'
            });
        } catch (err) {
            return res.status(401).json({
                message: 'unauthorized access',
                error: err
            });
        }
    }
};
