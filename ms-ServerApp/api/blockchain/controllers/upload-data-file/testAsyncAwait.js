const _ = require('lodash');

var arrayOfObj = [{
   fileName: 'IPFS CSV File - Sheet1.csv',
   fileSize: 127,
   ipfsHash: 'QmTNvFFYFK9AeFVbFrE76apjHNrdvowzR9HEN7kudMeYJh'
},
{
   fileName: 'mai_token.txt',
   fileSize: 969,
   ipfsHash: 'QmUY4HASSCHcP8p5MrSSSuesFQGY27dDHTYYB4W4mqxXDB'
},
{
   fileName: 'IPFS CSV File - Sheet1.csv',
   fileSize: 127,
   ipfsHash: 'QmTNvFFYFK9AeFVbFrE76apjHNrdvowzR9HEN7kudMeYJh'
},
{
   fileName: 'sampleCSV.csv',
   fileSize: 151,
   ipfsHash: 'QmWpNynt8oqsFQGZDn9NZjJS2KCdcGLgYm2EBV4HqgeYJF'
},
{
   fileName: 'mai_token.txt',
   fileSize: 969,
   ipfsHash: 'QmUY4HASSCHcP8p5MrSSSuesFQGY27dDHTYYB4W4mqxXDB'
},
{
   fileName: 'sample2CSV.csv',
   fileSize: 167,
   ipfsHash: 'QmSfbmzjw1ccuVLoCTzmBAR1wGPZYb7jzeHoZRM33xAYXW'
},
{
   fileName: 'mai_token.txt',
   fileSize: 969,
   ipfsHash: 'QmUY4HASSCHcP8p5MrSSSuesFQGY27dDHTYYB4W4mqxXDB'
},
{
   fileName: 'faizuloffice@gmail.com.txt',
   fileSize: 993,
   ipfsHash: 'Qmb5ZBsDyo8z39uMkVUXMxbwQyy2BA62BaBFAStBL6cHyn'
},
{
   fileName: 'faizuloffice@gmail.com.txt',
   fileSize: 993,
   ipfsHash: 'Qmb5ZBsDyo8z39uMkVUXMxbwQyy2BA62BaBFAStBL6cHyn'
},
{
   fileName: 'faizuloffice@gmail.com.txt',
   fileSize: 993,
   ipfsHash: 'Qmb5ZBsDyo8z39uMkVUXMxbwQyy2BA62BaBFAStBL6cHyn'
},
{
   fileName: 'photo-1530212486532-d957d9c01d3e.jpeg',
   fileSize: 148588,
   ipfsHash: 'QmSHt2gmNQwDmQ1aG7PdYwji97nt5ZLopgaT64fyPZ9MeT'
},
{
   fileName: 'photo-1530212486532-d957d9c01d3e.jpeg',
   fileSize: 148588,
   ipfsHash: 'QmSHt2gmNQwDmQ1aG7PdYwji97nt5ZLopgaT64fyPZ9MeT'
},
{
   fileName: 'photo-1530212486532-d957d9c01d3e.jpeg',
   fileSize: 148588,
   ipfsHash: 'QmSHt2gmNQwDmQ1aG7PdYwji97nt5ZLopgaT64fyPZ9MeT',
   csvRows: 561,
   csvColumns: 1
},
{
   fileName: 'faizul.csv',
   fileSize: 135,
   ipfsHash: 'QmeAprfKpT7Y2LXM45tFgU6GCSHtHMgvbJjdRVVNfTwpW1',
   csvRows: 4,
   csvColumns: 3
},
{
   fileName: 'faizul.csv',
   fileSize: 150,
   ipfsHash: 'QmbhkFJXj8f9ogscNZzkLQJpahrEt7X5164XUyJCVq5odP',
   csvRows: 5,
   csvColumns: 3
},
{
   fileName: 'faizul.csv',
   fileSize: 150,
   ipfsHash: 'QmbhkFJXj8f9ogscNZzkLQJpahrEt7X5164XUyJCVq5odP',
   csvRows: 5,
   csvColumns: 3
},
{
   fileName: 'faizul.csv',
   fileSize: 150,
   ipfsHash: 'QmbhkFJXj8f9ogscNZzkLQJpahrEt7X5164XUyJCVq5odP',
   csvRows: 5,
   csvColumns: 3
},
{
   fileName: 'faizul.csv',
   fileSize: 150,
   ipfsHash: 'QmbhkFJXj8f9ogscNZzkLQJpahrEt7X5164XUyJCVq5odP',
   csvRows: 5,
   csvColumns: 3
},
{
   fileName: 'sample2CSV.csv',
   fileSize: 200,
   ipfsHash: 'QmR7mrrpoMY56fGEuQEfyuv15mqEnPesoPir8kbqxCzYXk',
   csvRows: 2,
   csvColumns: 3
},
{
   fileName: 'sample2CSV.csv',
   fileSize: 200,
   ipfsHash: 'QmR7mrrpoMY56fGEuQEfyuv15mqEnPesoPir8kbqxCzYXk',
   csvRows: 2,
   csvColumns: 3
},
{
   fileName: 'sample2CSV.csv',
   fileSize: 200,
   ipfsHash: 'QmR7mrrpoMY56fGEuQEfyuv15mqEnPesoPir8kbqxCzYXk',
   csvRows: 2,
   csvColumns: 3
},
{
   fileName: 'sample2CSV.csv',
   fileSize: 200,
   ipfsHash: 'QmR7mrrpoMY56fGEuQEfyuv15mqEnPesoPir8kbqxCzYXk',
   csvRows: 2,
   csvColumns: 3
},
{
   fileName: 'sample2CSV.csv',
   fileSize: 200,
   ipfsHash: 'QmR7mrrpoMY56fGEuQEfyuv15mqEnPesoPir8kbqxCzYXk',
   csvRows: 2,
   csvColumns: 3
},
{
   fileName: 'SampleCSVFile_11kb.csv',
   fileSize: 10998,
   ipfsHash: 'QmZwho3xrCV3z6aR16pSSWCepbTSwG8xahDkjMMS9VJerW',
   csvRows: 99,
   csvColumns: 10,
   fileEndpoint: 'http://localhost:3300/csv_dataFiles/users/Qmb5ZBsDyo8z39uMkVUXMxbwQyy2BA62BaBFAStBL6cHyn/SampleCSVFile_11kb.csv'
},
{
   fileName: 'SampleCSVFile_11kb.csv',
   fileSize: 10998,
   ipfsHash: 'QmZwho3xrCV3z6aR16pSSWCepbTSwG8xahDkjMMS9VJerW',
   csvRows: 99,
   csvColumns: 10,
   fileEndpoint: 'http://localhost:3300/csv_dataFiles/users/Qmb5ZBsDyo8z39uMkVUXMxbwQyy2BA62BaBFAStBL6cHyn/SampleCSVFile_11kb.csv'
}];


var filtered = arrayOfObj.filter(function(value, index, arr){

   return arr[index]['fileName'] == 'SampleCSVFile_11kb.csv';

});

/* console.log(filtered); */


function arrayRemove(arr, value) {

   return arr.filter(function(ele){
       return ele['fileName'] != value;
   });

}

var result = arrayRemove(arrayOfObj, 'faizul.csv');

//console.log('result', result);

exports.filterUsingLodash = (req,res)=>{
   var arr = [
      {
         parentFileName: 'faizul.csv',
         children: [
            {
               fileName: 'ws_faizul_01.hex',
               txnID: 'id_1'
            },
            {
               fileName: 'ws_faizul_02.hex',
               txnID: 'id_2'
            }
         ]
      },
      {
         parentFileName: 'faizulhassan.csv',
         children: [
            {
               fileName: 'ws_faizulhassan_01.hex',
               txnID: 'id_1'
            },
            {
               fileName: 'ws_faizulhassan_02.hex',
               txnID: 'id_2'
            }
         ]
      },
      {
         parentFileName: 'hassan.csv',
         children: [
            {
               fileName: 'ws_hassan_01.hex',
               txnID: 'id_1'
            },
            {
               fileName: 'ws_hassan_02.hex',
               txnID: 'id_2'
            }
         ]
      }
   ]
   var picked = _.filter(arr, x => x.parentFileName === 'hassan.csv');
   console.log('picked::', picked);

   picked[0].children.push({
      fileName: 'ws_hassan_07.hex',
      txnID: 'id_7'
   });

   picked[0].children.push({
      fileName: 'ws_hassan_08.hex',
      txnID: 'id_8'
   });

   res.json({
      output: arr
   });
}