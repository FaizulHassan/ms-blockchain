const ipfsAPI = require('ipfs-api');
var fs = require('fs');
var multer = require('multer');

module.exports = {
   uploadDataFile: (req, res) => {
      var csvFilePath = req.file.path;
      console.log('csvFilePath', csvFilePath);
      fs.readFile(csvFilePath, (err, data) => {
         if (err) {
            res.status(400).json({
               error: err,
               message: 'error uploading file'
            });
         } else {
            let dataFileContent = data;

            //Connecting to the ipfs network via infura gateway
            const ipfs = ipfsAPI('13.250.116.159', '5001', { protocol: 'http' });
            ipfs.files.add(dataFileContent, (err, hash) => {
               if (err) {
                  console.log(err);
               }
               console.log('ipfs hash: ', hash);

               res.status(200).json({
                  ipfsHashOfFile: hash,
                  message: 'Upload Sucessful !'
               })
            });
         }
      });
   },


   getDataFileFromIPFSHash: (req, res) => {

      const ipfs = ipfsAPI('13.250.116.159', '5001', { protocol: 'http' });
      var dataFileHash = req.body.dataHash;

      ipfs.files.get(dataFileHash, function (err, files) {
         files.forEach(file => {
            console.log(file.path);
            console.log(file.content.toString('utf8'));

            res.status(200).json({
               dataFromIPFS: file.content.toString('utf8')
            });
         });
      });
   }
};
