const express = require('express');
const app = express();
var path = require('path');
var util = require('util');
var os = require('os');
const ipfsAPI = require('ipfs-api');

var Fabric_Client = require('fabric-client');

var DEBUG = 1;

// Console.log function
function consolelog(message, variable) {
   if (DEBUG == 1) console.log(message, variable);
}

module.exports = (function () {
   return {
      queryUploadedDataFile: function (req, res) {
         const orgTokenDetails = res.locals.orgDetails;
         console.log('orgTokenDetails:', orgTokenDetails);

         var orgKey = `uploadDataFiles_${orgTokenDetails._ipfsHash}`;
         // setup the fabric network
         var fabric_client = new Fabric_Client();
         var channel = fabric_client.newChannel('mychannel');
         var peer = fabric_client.newPeer('grpc://localhost:7051');
         channel.addPeer(peer);

         //
         var member_user = null;
         var store_path = path.join(
            os.homedir(),
            '.hfc-key-store/blockchain_keys'
         );
         console.log('Store path:' + store_path);
         var tx_id = null;

         // create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
         Fabric_Client.newDefaultKeyValueStore({
            path: store_path
         })
            .then(state_store => {
               // assign the store to the fabric client
               fabric_client.setStateStore(state_store);
               var crypto_suite = Fabric_Client.newCryptoSuite();
               // use the same location for the state store (where the users' certificate are kept)
               // and the crypto store (where the users' keys are kept)
               var crypto_store = Fabric_Client.newCryptoKeyStore({
                  path: store_path
               });
               crypto_suite.setCryptoKeyStore(crypto_store);
               fabric_client.setCryptoSuite(crypto_suite);

               // get the enrolled user from persistence, this user will sign all requests
               return fabric_client.getUserContext('admin', true);
            })
            .then(user_from_store => {
               if (user_from_store && user_from_store.isEnrolled()) {
                  console.log(
                     'Successfully loaded user from persistence'
                  );
                  member_user = user_from_store;
               } else {
                  throw new Error(
                     'Failed to get user.... run registerUser.js'
                  );
               }

               // queryTuna - requires 1 argument, ex: args: ['4'],
               const request = {
                  chaincodeId: 'mschaincode',
                  txId: tx_id,
                  fcn: 'queryUploadedDataFileHash',
                  args: [orgKey]
               };

               // send the query proposal to the peer
               return channel.queryByChaincode(request);
            })
            .then(query_responses => {
               console.log('Query has completed, checking results');
               // query_responses could have more than one  results if there multiple peers were used as targets
               if (query_responses && query_responses.length == 1) {
                  if (query_responses[0] instanceof Error) {
                     console.error(
                        'error from query = ',
                        query_responses[0]
                     );
                     res.status(400).json({
                        message: 'No data files found'
                     });
                  } else {
                     console.log(
                        'Response is ',
                        query_responses[0].toString()
                     );
                     var queryResult = query_responses[0].toString();
                     //res.send(query_responses[0].toString());
                     //console.log('QUERY RES', queryResult);

                     var parseResult = JSON.parse(queryResult);
                     var dataSet = JSON.parse(parseResult.dataSet);

                     console.log('parseResult:::', parseResult);
                     res.status(200).json({
                        dataSet: dataSet,
                        email: parseResult.representativeEmail
                     });
                  }
               } else {
                  console.log('No payloads were returned from query');
                  res.status(400).json({
                     message: 'Could not get data file'
                  });
               }
            })
            .catch(err => {
               console.error('Failed to query successfully :: ' + err);
               res.status(400).json({ message: 'Failed to query' });
            });
      }
   };
})();
