const ipfsAPI = require('ipfs-api');
var fs = require('fs');

var Fabric_Client = require('fabric-client');
var path = require('path');
var util = require('util');
var os = require('os');
const csv = require('csvtojson');
var mkdirp = require('mkdirp');


var bcDataSet = '';

exports.uploadDataFile = async (req, res, next) => {
   const orgTokenDetails = res.locals.orgDetails;
   console.log('orgTokenDetails:', orgTokenDetails);

   var csvRows = '';
   var csvColumns = '';
   var fileEndpoint = '';
   bcDataSet = '';

   var orgKey = `uploadDataFiles_${orgTokenDetails._ipfsHash}`;
   // setup the fabric network
   var fabric_client = new Fabric_Client();
   var channel = fabric_client.newChannel('mychannel');
   var peer = fabric_client.newPeer('grpc://localhost:7051');
   channel.addPeer(peer);

   //
   var member_user = null;
   var store_path = path.join(
      os.homedir(),
      '.hfc-key-store/blockchain_keys'
   );
   console.log('Store path:' + store_path);
   var tx_id = null;

   // create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
   Fabric_Client.newDefaultKeyValueStore({
      path: store_path
   })
      .then(state_store => {
         // assign the store to the fabric client
         fabric_client.setStateStore(state_store);
         var crypto_suite = Fabric_Client.newCryptoSuite();
         // use the same location for the state store (where the users' certificate are kept)
         // and the crypto store (where the users' keys are kept)
         var crypto_store = Fabric_Client.newCryptoKeyStore({
            path: store_path
         });
         crypto_suite.setCryptoKeyStore(crypto_store);
         fabric_client.setCryptoSuite(crypto_suite);

         // get the enrolled user from persistence, this user will sign all requests
         return fabric_client.getUserContext('admin', true);
      })
      .then(user_from_store => {
         if (user_from_store && user_from_store.isEnrolled()) {
            console.log(
               'Successfully loaded user from persistence'
            );
            member_user = user_from_store;
         } else {
            throw new Error(
               'Failed to get user.... run registerUser.js'
            );
         }

         const request = {
            chaincodeId: 'mschaincode',
            txId: tx_id,
            fcn: 'queryUploadedDataFileHash',
            args: [orgKey]
         };

         // send the query proposal to the peer
         return channel.queryByChaincode(request);
      })
      .then(query_responses => {
         console.log('Query has completed, checking results');
         // query_responses could have more than one  results if there multiple peers were used as targets
         if (query_responses && query_responses.length == 1) {
            var queryResult = query_responses[0].toString();

            var parseResult = '';

            if (query_responses[0] instanceof Error) {
               console.log('Getting into If Statement');
            } else {
               console.log('else part');
               parseResult = JSON.parse(queryResult);
               bcDataSet = JSON.parse(parseResult.dataSet);
            }

            /* File Upload Part Starts */
            const orgTokenDetails = res.locals.orgDetails;
            console.log('orgTokenDetails:', orgTokenDetails);

            // getting org id of a user
            var orgID = orgTokenDetails._ipfsHash;
            var orgEmail = orgTokenDetails._orgMailId;
            var dataSet = [];
            var dataSetLength = '';

            // uploading Files to IPFS Starts

            if (req.file) {
               var csvFilePath = req.file.path;
            } else {
               res.status(404).json({
                  message: 'please upload a file'
               });
            }

            console.log('csvFilePath', req.file);
            fs.readFile(csvFilePath, (err, data) => {
               if (err) {
                  res.status(400).json({
                     error: err,
                     message: 'error uploading file'
                  });
               } else {

                  if (data) {

                     /* Create a directory to save user data file */
                     var userFilePath = `public/csv_dataFiles/users/${orgTokenDetails._ipfsHash}/details`
                     mkdirp(userFilePath, function (err) {
                        if (err) console.error(err)
                        else {
                           console.log('done creating directory');

                           fs.writeFile(`${userFilePath}/${req.file.originalname}`, data, (err) => {
                              if (err) {
                                 console.log('Error in writing file to the user directory');
                              } else {

                                 var hostName = req.headers.host;

                                 if (hostName == 'localhost:3300') {
                                    fileEndpoint = `http://localhost:3300/${userFilePath.split('public/')[1]}/${req.file.originalname}`;
                                 } else {
                                    fileEndpoint = `https://bc.askmai.io/${userFilePath.split('public/')[1]}/${req.file.originalname}`;

                                 }

                                 console.log('fileEndpoint path::', fileEndpoint);
                                 console.log('done writing file to user directory');

                                 /* Getting No. Of Rows & Columns */
                                 csv()
                                    .fromFile(csvFilePath)
                                    .then((jsonObj) => {
                                       console.log('Csv JSONObj::', jsonObj);

                                       csvRows = jsonObj.length;

                                       console.log('Csv Field Name', Object.keys(jsonObj[0]));

                                       csvColumns = Object.keys(jsonObj[0]).length;
                                    });

                                 console.log('console.log 2');
                                 let dataFileContent = data;

                                 //Connecting to the ipfs network via infura gateway
                                 const ipfs = ipfsAPI('13.250.116.159', '5001', { protocol: 'http' });
                                 ipfs.files.add(dataFileContent, (err, hash) => {
                                    if (err) {
                                       console.log(err);
                                    } else {
                                       //console.log('ipfs hash: ', hash);

                                       //console.log('BC DATA SET 1 Stringyfy:', JSON.stringify(bcDataSet).replace(/[\[\]']+/g,''));

                                       console.log('BC DATA SET 1:', bcDataSet);
                                       if (bcDataSet.length > 0) {
                                          var stringyfyData = JSON.stringify(bcDataSet).replace(/[\[\]']+/g, '');

                                          console.log('Array of Obj', stringyfyData);

                                          var addBrackets = `[${stringyfyData}]`

                                          console.log('Adding Brackets', addBrackets);
                                          var parseString = JSON.parse(addBrackets);

                                          dataSet.push(parseString);
                                          //console.log('If DataSet:', dataSet);
                                       }

                                       console.log('DataSet:::1', dataSet);

                                       if (dataSet.length == 0) {
                                          dataSet.push({
                                             fileName: req.file.originalname,
                                             fileSize: req.file.size,
                                             ipfsHash: hash[0].hash,
                                             csvRows: csvRows,
                                             csvColumns: csvColumns,
                                             fileEndpoint: fileEndpoint
                                          });

                                          dataSetLength = 1;
                                       } else {
                                          dataSet[0].push({
                                             fileName: req.file.originalname,
                                             fileSize: req.file.size,
                                             ipfsHash: hash[0].hash,
                                             csvRows: csvRows,
                                             csvColumns: csvColumns,
                                             fileEndpoint: fileEndpoint
                                          });

                                          dataSetLength = dataSet[0].length;
                                       }

                                       console.log('DataSet:::2', dataSet);

                                       console.log('Dataset length::', dataSet[0].length);

                                       /* dataSet.push({
                                          fileName: req.file.originalname,
                                          fileSize: req.file.size,
                                          ipfsHash: hash[0].hash
                                       }); */

                                       //console.log('DataSet:', dataSet);
                                       //console.log('DataSet String:', JSON.stringify(dataSet));
                                       /* res.status(200).json({
                                          ipfsHashOfFile: hash,
                                          message: 'Upload Sucessful !'
                                       }) */

                                       var fabric_client = new Fabric_Client();

                                       // setup the fabric network
                                       var channel = fabric_client.newChannel('mychannel');
                                       var peer = fabric_client.newPeer('grpc://localhost:7051');
                                       channel.addPeer(peer);
                                       var order = fabric_client.newOrderer('grpc://localhost:7050')
                                       channel.addOrderer(order);

                                       var member_user = null;
                                       var store_path = path.join(
                                          os.homedir(),
                                          '.hfc-key-store/blockchain_keys'
                                       );
                                       //console.log('Store path:' + store_path);
                                       var tx_id = null;

                                       // create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
                                       Fabric_Client.newDefaultKeyValueStore({
                                          path: store_path
                                       }).then((state_store) => {
                                          // assign the store to the fabric client
                                          fabric_client.setStateStore(state_store);
                                          var crypto_suite = Fabric_Client.newCryptoSuite();
                                          // use the same location for the state store (where the users' certificate are kept)
                                          // and the crypto store (where the users' keys are kept)
                                          var crypto_store = Fabric_Client.newCryptoKeyStore({ path: store_path });
                                          crypto_suite.setCryptoKeyStore(crypto_store);
                                          fabric_client.setCryptoSuite(crypto_suite);

                                          // get the enrolled user from persistence, this user will sign all requests
                                          return fabric_client.getUserContext('admin', true);
                                       }).then((user_from_store) => {
                                          if (user_from_store && user_from_store.isEnrolled()) {
                                             console.log('Successfully loaded admin from persistence');
                                             member_user = user_from_store;
                                          } else {
                                             throw new Error('Failed to get admin.... run registerUser.js');
                                          }

                                          // get a transaction id object based on the current user assigned to fabric client
                                          tx_id = fabric_client.newTransactionID();
                                          console.log("Assigning transaction_id: ", tx_id._transaction_id);

                                          // createCar chaincode function - requires 5 args, ex: args: ['CAR12', 'Honda', 'Accord', 'Black', 'Tom'],
                                          // changeCarOwner chaincode function - requires 2 args , ex: args: ['CAR10', 'Dave'],
                                          // must send the proposal to endorsing peers
                                          var request = {
                                             //targets: let default to the peer assigned to the client
                                             chaincodeId: 'mschaincode',
                                             fcn: 'uploadDataFile',
                                             args: [
                                                orgID,
                                                orgEmail,
                                                JSON.stringify(dataSet)
                                             ],
                                             chainId: 'mychannel',
                                             txId: tx_id
                                          };

                                          // send the transaction proposal to the peers
                                          return channel.sendTransactionProposal(request);
                                       }).then((results) => {
                                          var proposalResponses = results[0];
                                          var proposal = results[1];
                                          let isProposalGood = false;
                                          if (proposalResponses && proposalResponses[0].response &&
                                             proposalResponses[0].response.status === 200) {
                                             isProposalGood = true;
                                             console.log('Transaction proposal was good');
                                          } else {
                                             console.error('Transaction proposal was bad');
                                          }
                                          if (isProposalGood) {
                                             console.log(util.format(
                                                'Successfully sent Proposal and received ProposalResponse: Status - %s, message - "%s"',
                                                proposalResponses[0].response.status, proposalResponses[0].response.message));

                                             // build up the request for the orderer to have the transaction committed
                                             var request = {
                                                proposalResponses: proposalResponses,
                                                proposal: proposal
                                             };

                                             // set the transaction listener and set a timeout of 30 sec
                                             // if the transaction did not get committed within the timeout period,
                                             // report a TIMEOUT status
                                             var transaction_id_string = tx_id.getTransactionID(); //Get the transaction ID string to be used by the event processing
                                             var promises = [];

                                             var sendPromise = channel.sendTransaction(request);
                                             promises.push(sendPromise); //we want the send transaction first, so that we know where to check status

                                             // get an eventhub once the fabric client has a user assigned. The user
                                             // is required bacause the event registration must be signed
                                             let event_hub = channel.newChannelEventHub(peer);

                                             // using resolve the promise so that result status may be processed
                                             // under the then clause rather than having the catch clause process
                                             // the status
                                             let txPromise = new Promise((resolve, reject) => {
                                                let handle = setTimeout(() => {
                                                   event_hub.unregisterTxEvent(transaction_id_string);
                                                   event_hub.disconnect();
                                                   resolve({ event_status: 'TIMEOUT' }); //we could use reject(new Error('Trnasaction did not complete within 30 seconds'));
                                                }, 3000);
                                                event_hub.registerTxEvent(transaction_id_string, (tx, code) => {
                                                   // this is the callback for transaction event status
                                                   // first some clean up of event listener
                                                   clearTimeout(handle);

                                                   // now let the application know what happened
                                                   var return_status = { event_status: code, tx_id: transaction_id_string };
                                                   if (code !== 'VALID') {
                                                      console.error('The transaction was invalid, code = ' + code);
                                                      resolve(return_status); // we could use reject(new Error('Problem with the tranaction, event status ::'+code));
                                                   } else {
                                                      console.log('The transaction has been committed on peer ' + event_hub.getPeerAddr());
                                                      resolve(return_status);
                                                   }
                                                }, (err) => {
                                                   //this is the callback if something goes wrong with the event registration or processing
                                                   reject(new Error('There was a problem with the eventhub ::' + err));
                                                },
                                                   { disconnect: true } //disconnect when complete
                                                );
                                                event_hub.connect();

                                             });
                                             promises.push(txPromise);

                                             return Promise.all(promises);
                                          } else {
                                             console.error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
                                             throw new Error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
                                          }
                                       }).then((results) => {
                                          console.log('Send transaction promise and event listener promise have completed');
                                          // check the results in the order the promises were added to the promise all list
                                          if (results && results[0] && results[0].status === 'SUCCESS') {
                                             console.log('Successfully sent transaction to the orderer.');
                                          } else {
                                             console.error('Failed to order the transaction. Error code: ' + results[0].status);
                                          }

                                          if (results && results[1] && results[1].event_status === 'VALID') {
                                             console.log('Successfully committed the change to the ledger by the peer');
                                             res.status(200).json({
                                                message: 'File upload to IPFS Successful!',
                                                totalFilesUploaded: dataSetLength
                                             });

                                          } else {
                                             console.log('Transaction failed to be committed to the ledger due to ::' + results[1].event_status);
                                          }
                                       }).catch((err) => {
                                          console.error('Failed to invoke successfully :: ' + err);
                                       });
                                    }

                                 });


                              }
                           })
                        }
                     });
                  }
               }
            });

            /* File Upload Part Ends */
         } else {
            console.log('No payloads were returned from query');
            console.log({
               message: 'Could not get data file'
            });
         }
      })
      .catch(err => {
         console.error('Failed to query successfully :: ' + err);

         console.log({ message: 'Failed to query' })
      });
}

