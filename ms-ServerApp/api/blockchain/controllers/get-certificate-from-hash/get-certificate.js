const ipfsAPI = require('ipfs-api');
var fs = require('fs');

var dataToWrite = '';

var ipfsHash = '';

module.exports = {
    getCertFromHash: (req, res) => {
        ipfsHash = req.body.ipfsHash;

        //Connceting to the ipfs network via infura gateway
        const ipfs = ipfsAPI('ipfs.infura.io', '5001', { protocol: 'https' });

        ipfs.files.get(ipfsHash, function(err, files) {
            if (err) {
                console.log('Certificate not found', err);
                res.status(404).json({
                    message: 'Certificate not found'
                });
            }
            if (files) {
                files.forEach(file => {
                    console.log(file.path);
                    console.log(file.content.toString('utf8'));

                    dataToWrite = file.content.toString('utf8');

                    fs.writeFile(
                        'public/certificate/certificate.txt',
                        dataToWrite,
                        function(err, data) {
                            if (err) console.log(err);
                            console.log('Successfully Written to File.');
                        }
                    );

                    res.status(200).json({
                        certificate: file.content.toString('utf8')
                    });
                });
            }
        });
    }
};
