var path = require('path');
var util = require('util');
var os = require('os');
var fs = require('fs');
var path = require('path');

var Fabric_Client = require('fabric-client');
var Fabric_CA_Client = require('fabric-ca-client');
const ipfsAPI = require('ipfs-api');
const async = require('async');

var ctrlAddOrgs = require('../add-orgs-details/add-orgs-details');

//Connceting to the ipfs network via infura gateway
const ipfs = ipfsAPI('ipfs.infura.io', '5001', { protocol: 'https' });

var dataToWrite = '';

module.exports = {
    registerOrgs: (req, res) => {
        var representativeEmail = req.body.representativeEmail;

        // Removing the current Cert before proceeding
        fs.exists('public/certificate/'+representativeEmail+'.txt', function(exists) {
            if (exists) {
                fs.unlink('public/certificate/'+representativeEmail+'.txt');
                console.log('cert removed');
            }else{
                console.log('file not present');
            }
        });        

        var fabric_client = new Fabric_Client();
        var fabric_ca_client = null;
        var admin_user = null;
        var member_user = null;
        var store_path = path.join(
            os.homedir(),
            '.hfc-key-store/blockchain_keys'
        );

        var uniqueId = '';
        var userCert = '';
        var ipfsHash = '';

        console.log(' Store path:' + store_path);

        // create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
        Fabric_Client.newDefaultKeyValueStore({ path: store_path })
            .then(state_store => {
                console.log('state store', state_store);
                // assign the store to the fabric client
                fabric_client.setStateStore(state_store);
                var crypto_suite = Fabric_Client.newCryptoSuite();
                // use the same location for the state store (where the users' certificate are kept)
                // and the crypto store (where the users' keys are kept)
                var crypto_store = Fabric_Client.newCryptoKeyStore({
                    path: store_path
                });
                crypto_suite.setCryptoKeyStore(crypto_store);
                fabric_client.setCryptoSuite(crypto_suite);
                var tlsOptions = {
                    trustedRoots: [],
                    verify: false
                };
                // be sure to change the http to https when the CA is running TLS enabled
                fabric_ca_client = new Fabric_CA_Client(
                    'http://localhost:7054',
                    null,
                    '',
                    crypto_suite
                );

                // first check to see if the admin is already enrolled
                return fabric_client.getUserContext('admin', true);
            })
            .then(user_from_store => {
                if (user_from_store && user_from_store.isEnrolled()) {
                    console.log('Successfully loaded admin from persistence');
                    admin_user = user_from_store;
                } else {
                    throw new Error(
                        'Failed to get admin.... run enrollAdmin.js'
                    );
                }

                // at this point we should have the admin user
                // first need to register the user with the CA server
                return fabric_ca_client.register(
                    {
                        enrollmentID: representativeEmail,
                        affiliation: 'org1.department1',
                        role: 'client'
                    },
                    admin_user
                );
            })
            .then(secret => {
                uniqueId = secret;
                // next we need to enroll the user with CA server
                console.log(
                    'Successfully registered ' +
                        representativeEmail +
                        ' - secret:' +
                        secret
                );

                return fabric_ca_client.enroll({
                    enrollmentID: representativeEmail,
                    enrollmentSecret: secret
                });
            })
            .then(enrollment => {
                console.log(
                    'Successfully enrolled member user "' +
                        representativeEmail +
                        '" '
                );
                userCert = enrollment.certificate;
                console.log('CERTIFICATE:', userCert);

                return fabric_client.createUser({
                    username: representativeEmail,
                    mspid: 'Org1MSP',
                    cryptoContent: {
                        privateKeyPEM: enrollment.key.toBytes(),
                        signedCertPEM: enrollment.certificate
                    }
                });
            })
            .then(user => {
                member_user = user;

                return fabric_client.setUserContext(member_user);
            })
            .then(() => {
                console.log(
                    'User was successfully registered and enrolled and is ready to interact with the fabric network'
                );

                async.waterfall(
                    [
                        function(done) {
                            //ctrlAddOrgs.addOrgDetails(req, res);
                            done(null, 'func1');
                        },
                        function(value1, done) {
                            console.log('value1', value1);
                            function sendHashtoIPFS() {
                                let userCertBuffer = new Buffer(userCert);
                                ipfs.files.add(userCertBuffer, (err, hash) => {
                                    if (err) {
                                        console.log(err);
                                    }
                                    if (hash) {
                                        dataToWrite = userCert;

                                        fs.writeFile(
                                            'public/certificate/'+representativeEmail+'.txt',
                                            dataToWrite,
                                            function(err, data) {
                                                if (err) console.log(err);
                                                console.log(
                                                    'Successfully Written to File.'
                                                );
                                            }
                                        );

                                        ipfsHash = hash[0].hash;
                                        console.log('ipfs cert hash: ', hash);
                                        console.log('IPFS Hash: ', ipfsHash);
                                        ctrlAddOrgs.addOrgDetails(
                                            req,
                                            res,
                                            ipfsHash
                                        );
                                        res.status(200).json({
                                            message:
                                                'Successfully Registered ' +
                                                representativeEmail +
                                                ' and is ready to interact with the fabric network',
                                            certificate: userCert,
                                            //uniqueKey: uniqueId,
                                            ipfsHash: ipfsHash
                                        });
                                    }
                                });

                                return ipfsHash;
                            }

                            done(null, sendHashtoIPFS());
                        }
                    ],
                    function(err) {
                        if (err) throw new Error(err);
                    }
                );
            })
            .catch(err => {
                console.error('Failed to register: ' + err);

                if (err.toString().indexOf('is already registered') != -1) {
                    res.status(500).json({
                        error: 'User already registered'
                    });
                }

                if (err.toString().indexOf('Authorization') > -1) {
                    console.error(
                        'Authorization failures may be caused by having admin credentials from a previous CA instance.\n' +
                            'Try again after deleting the contents of the store directory ' +
                            store_path
                    );
                }
            });
    }
};
