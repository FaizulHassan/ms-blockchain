const ipfsAPI = require('ipfs-api');

var userCert = `
-----BEGIN CERTIFICATE-----\nMIICpzCCAk2gAwIBAgIUThBIbHi4FWkWWK4KIiuWyl1bHyowCgYIKoZIzj0EAwIw\nczELMAkGA1UEBhMCVVMxEzARBgNVBAgTCkNhbGlmb3JuaWExFjAUBgNVBAcTDVNh\nbiBGcmFuY2lzY28xGTAXBgNVBAoTEG9yZzEuZXhhbXBsZS5jb20xHDAaBgNVBAMT\nE2NhLm9yZzEuZXhhbXBsZS5jb20wHhcNMTgxMjE0MTQxMzAwWhcNMTkxMjE0MTQx\nODAwWjBOMTAwDQYDVQQLEwZjbGllbnQwCwYDVQQLEwRvcmcxMBIGA1UECxMLZGVw\nYXJ0bWVudDExGjAYBgNVBAMMEXlhaG9vaEZAYXBwbGUuY29tMFkwEwYHKoZIzj0C\nAQYIKoZIzj0DAQcDQgAEBk01HlJ9joUoE6LqJQhDNYl5fExhrVd9IjXi+6e1UWow\n8E6oBVrMPX3PSU5eDfsJT5FhLQmsKeTBgcZHNTL8/qOB4zCB4DAOBgNVHQ8BAf8E\nBAMCB4AwDAYDVR0TAQH/BAIwADAdBgNVHQ4EFgQUTC1txLbRQ5ZrZmhtPgPajEpG\nimYwKwYDVR0jBCQwIoAgQjmqDc122u64ugzacBhR0UUE0xqtGy3d26xqVzZeSXww\ndAYIKgMEBQYHCAEEaHsiYXR0cnMiOnsiaGYuQWZmaWxpYXRpb24iOiJvcmcxLmRl\ncGFydG1lbnQxIiwiaGYuRW5yb2xsbWVudElEIjoieWFob29oRkBhcHBsZS5jb20i\nLCJoZi5UeXBlIjoiY2xpZW50In19MAoGCCqGSM49BAMCA0gAMEUCIQD187PdGLOl\nScKjrtilc+UlcmzWnWSLfS2WNjDri2b71AIgKlF0mFWZMNoIKKbTUMd0qcIkOvuB\namU1oJErVHbVCzM=\n-----END CERTIFICATE-----\n
`;

let userCertBuffer = new Buffer(userCert);

//Connceting to the ipfs network via infura gateway
const ipfs = ipfsAPI('ipfs.infura.io', '5001', { protocol: 'https' });
ipfs.files.add(userCertBuffer, (err, hash) => {
    if (err) {
        console.log(err);
    }
    console.log('ipfs cert hash: ', hash);
});

var cerHash = 'QmR275caEcEhJFHbtgFJFnmkHDUf76GL5shUs5hzLT8GDg';

ipfs.files.get(cerHash, function(err, files) {
    files.forEach(file => {
        console.log(file.path);
        console.log(file.content.toString('utf8'));
    });
});
