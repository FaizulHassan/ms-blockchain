var fs = require('fs');
var path = require('path');
var os = require('os');
var fs = require('fs');

var ipfsAPI = require('ipfs-api');
var Fabric_Client = require('fabric-client');
const jwt = require('jsonwebtoken');
var emailConfig = require('../../../controllers/email-config/email-token');

var ipfs = ipfsAPI('ipfs.infura.io', '5001', { protocol: 'https' });

var ipfsHash = '';
var queryResponseEmail = '';
var readCertFromFile = '';

module.exports = {
    validateCertificate: (req, res) => {
        var userCertPath = req.file.path;

        fs.readFile(userCertPath, function (err, buf) {
            //console.log('CertInfo:',buf.toString());
            readCertFromFile = buf.toString();

            if (readCertFromFile) {
                var userCert = readCertFromFile;

                console.log('User Certificate:', userCert);

                let userCertBuffer = new Buffer(userCert);

                // GET IPFS CERT HASH

                ipfs.files.add(userCertBuffer, (err, hash) => {
                    if (err) {
                        console.log('err', err);
                    }
                    if (hash) {
                        ipfsHash = hash[0].hash;

                        // Blockchain Query Part Starts
                        /* Query Using Org ID Starts */
                        var orgID = ipfsHash;

                        console.log('orgID: ', orgID);

                        // setup the fabric network
                        var fabric_client = new Fabric_Client();
                        var channel = fabric_client.newChannel('mychannel');
                        var peer = fabric_client.newPeer(
                            'grpc://localhost:7051'
                        );
                        channel.addPeer(peer);

                        //
                        var member_user = null;
                        var store_path = path.join(
                            os.homedir(),
                            '.hfc-key-store/blockchain_keys'
                        );
                        console.log('Store path:' + store_path);
                        var tx_id = null;

                        // create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
                        Fabric_Client.newDefaultKeyValueStore({
                            path: store_path
                        })
                            .then(state_store => {
                                // assign the store to the fabric client
                                fabric_client.setStateStore(state_store);
                                var crypto_suite = Fabric_Client.newCryptoSuite();
                                // use the same location for the state store (where the users' certificate are kept)
                                // and the crypto store (where the users' keys are kept)
                                var crypto_store = Fabric_Client.newCryptoKeyStore(
                                    {
                                        path: store_path
                                    }
                                );
                                crypto_suite.setCryptoKeyStore(crypto_store);
                                fabric_client.setCryptoSuite(crypto_suite);

                                // get the enrolled user from persistence, this user will sign all requests
                                return fabric_client.getUserContext(
                                    'admin',
                                    true
                                );
                            })
                            .then(user_from_store => {
                                if (
                                    user_from_store &&
                                    user_from_store.isEnrolled()
                                ) {
                                    console.log(
                                        'Successfully loaded user from persistence'
                                    );
                                    member_user = user_from_store;
                                } else {
                                    throw new Error(
                                        'Failed to get user.... run registerUser.js'
                                    );
                                }

                                const request = {
                                    chaincodeId: 'mschaincode',
                                    txId: tx_id,
                                    fcn: 'queryOrganizationDetails',
                                    args: [orgID]
                                };

                                // send the query proposal to the peer
                                return channel.queryByChaincode(request);
                            })
                            .then(query_responses => {
                                /* queryResponseEmail =
                            query_responses[0].toString();
                        var JSONResMail = JSON.parse(queryResponseEmail).representativeEmail
                            console.log('queryResponseEmail: ',JSONResMail.representativeEmail);

                        var orgToken = jwt.sign(
                            {
                                _orgMailId: JSONResMail
                            },
                            'jwt_Secret_Key_for_Moolah_Sense_Of_32Bit_String',
                            {
                                expiresIn: '10950d'
                            }
                        );

                        const tokenArg = {
                            toEmail: JSONResMail,
                            token: orgToken,
                        };

                        if (orgToken) {
                            console.log('Token Generated');
                            emailConfig.emailToken(tokenArg);
                            console.log('Mail Sent Successful');
                        } */

                                console.log(
                                    'Query has completed, checking results'
                                );
                                // query_responses could have more than one  results if there multiple peers were used as targets
                                if (
                                    query_responses &&
                                    query_responses.length == 1
                                ) {
                                    if (query_responses[0] instanceof Error) {
                                        console.error(
                                            'error from query = ',
                                            query_responses[0]
                                        );
                                        res.send('Could not query for user');
                                    } else {
                                        console.log(
                                            'Response is ',
                                            query_responses[0].toString()
                                        );


                                        //res.send(query_responses[0].toString());

                                        // Displaying Access Revoked Message
                                        var finalResult = query_responses[0].toString();
                                        var isRevokedValue = JSON.parse(finalResult).isRevoked;
                                        if (isRevokedValue == 'true') {
                                            res.status(404).json({
                                                message: 'Your access has been revoked. Contact Admin for further assistance',
                                                statusCode: 404
                                            });
                                        } else {
                                            queryResponseEmail = query_responses[0].toString();
                                            var JSONResMail = JSON.parse(
                                                queryResponseEmail
                                            ).representativeEmail;
                                            console.log(
                                                'queryResponseEmail: ',
                                                JSONResMail
                                            );

                                            var orgToken = jwt.sign(
                                                {
                                                    _orgMailId: JSONResMail,
                                                    _ipfsHash: ipfsHash
                                                },
                                                'jwt_Secret_Key_for_Moolah_Sense_Of_32Bit_String',
                                                {
                                                    expiresIn: '18000000000'
                                                }
                                            );

                                            const tokenArg = {
                                                toEmail: JSONResMail,
                                                token: orgToken
                                            };

                                            if (orgToken) {
                                                console.log('Token Generated');
                                                emailConfig.emailToken(tokenArg);
                                                console.log('Mail Sent Successful');
                                            }

                                            // User Session Variable

                                            req.session.userEmail = JSONResMail;
                                            console.log('User Session Mail ID::', req.session.userEmail);

                                            res.status(200).json({
                                                message:
                                                    'Token sent to your registered eMail ' +
                                                    JSONResMail
                                            });
                                        }


                                    }
                                } else {
                                    console.log(
                                        'No payloads were returned from query'
                                    );
                                    res.send('Could not locate the product');
                                }
                            })
                            .catch(err => {
                                console.error(
                                    'Failed to query successfully :: ' + err
                                );
                                res.send('Could not locate the product');
                            });
                    }
                });
            }
        });
    }
};
