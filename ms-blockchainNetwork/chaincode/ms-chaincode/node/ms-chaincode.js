'use strict';
const shim = require('fabric-shim');
const util = require('util');

var DEBUG = 1;

// Console.log Utility function
function consolelog(message, variable) {
    if (DEBUG == 1) console.log(message, variable);
}

let Chaincode = class {
    // The Init method is called when the Smart Contract is instantiated by the blockchain network
    // Best practice is to have any Ledger initialization in separate function -- see initLedger()
    async Init(stub) {
        console.info('=========== Instantiated Chaincode: ===========');
        return shim.success();
    }

    // The Invoke method is called as a result of an application request to run the Smart Contract.
    // The calling application program has also specified the particular smart contract
    // function to be called, with arguments
    async Invoke(stub) {
        let ret = stub.getFunctionAndParameters();
        console.info(ret);

        let method = this[ret.fcn];
        if (!method) {
            console.error('no function of name:' + ret.fcn + ' found');
            throw new Error(
                'Received unknown function ' + ret.fcn + ' invocation'
            );
        }
        try {
            let payload = await method(stub, ret.params, this);
            return shim.success(payload);
        } catch (err) {
            console.log(err);
            return shim.error(err);
        }
    }

    async initLedger(stub, args) {

    }

    // Function to add assets. This function will be used by Asset's initial owner to register their product
    async orgSignupDetails(stub, args) {
        console.info('============= START : Adding Org Details ===========');
        console.log('Length:', args.length);
        /* if (args.length != 5) {
            throw new Error('Incorrect number of arguments. Expecting 5');
        } */

        var orgSignup = {
            companyName: args[0],
            address: args[1],
            zip: args[2],
            city: args[3],
            state: args[4],
            country: args[5],
            url: args[6],
            representativeName: args[7],
            representativeEmail: args[8],
            representativeMobile: args[9],
            orgCertHash: args[10],
            bigChainKeys: args[11],
            isRevoked: args[12]
        }

        await stub.putState(args[10], Buffer.from(JSON.stringify(orgSignup)));
        console.info('============= END : Adding Org Details ===========');
    }

    async updateOrganizationDetails(stub, args) {
        console.info('============= START : updateOrg Details ===========');
        /* if (args.length != 2) {
            throw new Error('Incorrect number of arguments. Expecting 2');
        } */

        let getOrgDetails = await stub.getState(args[0]);
        let orgData = JSON.parse(getOrgDetails);

        if (args[1] != '' && args[1] != undefined) {
            orgData.companyName = args[1];
        }
        if (args[2] != '' && args[2] != undefined) {
            orgData.address = args[2];
        }
        if (args[3] != '' && args[3] != undefined) {
            orgData.zip = args[3];
        }
        if (args[4] != '' && args[4] != undefined) {
            orgData.city = args[4];
        }
        if (args[5] != '' && args[5] != undefined) {
            orgData.state = args[5];
        }
        if (args[6] != '' && args[6] != undefined) {
            orgData.country = args[6];
        }
        if (args[7] != '' && args[7] != undefined) {
            orgData.url = args[7];
        }
        if (args[8] != '' && args[8] != undefined) {
            orgData.representativeName = args[8];
        }
        if (args[9] != '' && args[9] != undefined) {
            orgData.representativeEmail = args[9];
        }
        if (args[10] != '' && args[10] != undefined) {
            orgData.representativeMobile = args[10];
        }

        await stub.putState(args[0], Buffer.from(JSON.stringify(orgData)));
        console.info('============= END : updateOrg Details ===========');
    }

    //This is a query of Signed up Orgs details
    async queryOrganizationDetails(stub, args) {
        console.log('Entered Querying Assets');

        if (args.length != 1) {
            throw new Error(
                'Incorrect number of arguments. Expecting representativeEmail ex: roger@moolahsense.com'
            );
        }
        let representativeEmail = args[0];

        let orgDetailsAsBytes = await stub.getState(representativeEmail); //get the car from chaincode state
        if (!orgDetailsAsBytes || orgDetailsAsBytes.toString().length <= 0) {
            throw new Error(representativeEmail + ' does not exist: ');
        }
        consolelog('======== orgDetailsAsBytes ======== :\n', orgDetailsAsBytes);
        consolelog(
            '======== orgDetailsAsBytes to String ======== :\n',
            orgDetailsAsBytes.toString()
        );
        return orgDetailsAsBytes;
    }

    async queryAllAssets(stub, args) {
        let startKey = '0';
        let endKey = '999';

        let iterator = await stub.getStateByRange(startKey, endKey);

        consolelog('===========Iterator===========', iterator);

        let allResults = [];
        while (true) {
            let res = await iterator.next();
            consolelog('===========Res===========', res);

            if (res.value && res.value.value.toString()) {
                let jsonRes = {};
                console.log(res.value.value.toString('utf8'));

                jsonRes.Key = res.value.key;
                try {
                    jsonRes.Record = JSON.parse(
                        res.value.value.toString('utf8')
                    );
                } catch (err) {
                    console.log(err);
                    jsonRes.Record = res.value.value.toString('utf8');
                }
                allResults.push(jsonRes);
            }
            if (res.done) {
                console.log('end of data');
                await iterator.close();
                consolelog('=========== allResults ===========', 'res');
                console.info(allResults);
                return Buffer.from(JSON.stringify(allResults));
            }
        }
    }

    async uploadDataFile(stub, args) {
        console.info('============= START : Upload Data Files ===========');
        console.log('Length:', args.length);
        /* if (args.length != 5) {
            throw new Error('Incorrect number of arguments. Expecting 5');
        } */

        var uploadDataFiles = {
            organizationID: args[0],
            representativeEmail: args[1],
            dataSet: args[2]
        }

        await stub.putState('uploadDataFiles_' + args[0], Buffer.from(JSON.stringify(uploadDataFiles)));
        console.info('============= END : Upload Data Files ===========');
    }

    

    async queryUploadedDataFileHash(stub, args) {
        console.log('Entered Querying Assets');

        if (args.length != 1) {
            throw new Error(
                'Incorrect number of arguments. Expecting organizationID ex: qwewrewrewfewdfsfdgasdhfgdwerew'
            );
        }
        let organizationID = args[0];

        let orgDetailsAsBytes = await stub.getState(organizationID);
        if (!orgDetailsAsBytes || orgDetailsAsBytes.toString().length <= 0) {
            throw new Error(organizationID + ' does not exist: ');
        }
        consolelog('======== orgDetailsAsBytes ======== :\n', orgDetailsAsBytes);
        consolelog(
            '======== orgDetailsAsBytes to String ======== :\n',
            orgDetailsAsBytes.toString()
        );
        return orgDetailsAsBytes;
    }

    async saveModifiedData(stub, args) {
        console.info('============= START : Modified Data Files ===========');
        console.log('Length:', args.length);
        /* if (args.length != 5) {
            throw new Error('Incorrect number of arguments. Expecting 5');
        } */

        var modifiedDataFiles = {
            organizationID: args[0],
            wsDataSet: args[1]
        }

        await stub.putState('modifiedDataFiles_' + args[0], Buffer.from(JSON.stringify(modifiedDataFiles)));
        console.info('============= END : Modified Data Files ===========');
    }

    async queryModifiedDataFile(stub, args) {
        console.log('Entered Querying Modified Data');

        if (args.length != 1) {
            throw new Error(
                'Incorrect number of arguments. Expecting organizationID'
            );
        }
        let organizationID = args[0];

        let orgDetailsAsBytes = await stub.getState(organizationID);
        if (!orgDetailsAsBytes || orgDetailsAsBytes.toString().length <= 0) {
            throw new Error(organizationID + ' does not exist: ');
        }
        consolelog('======== orgDetailsAsBytes ======== :\n', orgDetailsAsBytes);
        consolelog(
            '======== orgDetailsAsBytes to String ======== :\n',
            orgDetailsAsBytes.toString()
        );
        return orgDetailsAsBytes;
    }

    async getHistoryForOrganization(stub, args, thisClass) {

        if (args.length < 1) {
            throw new Error('Incorrect number of arguments. Expecting 1')
        }
        let orgId = args[0];
        console.info('- start getHistoryForOrganization: %s\n', orgId);

        let resultsIterator = await stub.getHistoryForKey(orgId);
        let method = thisClass['getAllResults'];
        let results = await method(resultsIterator, true);

        return Buffer.from(JSON.stringify(results));
    }

    async getAllResults(iterator, isHistory) {
        let allResults = [];
        while (true) {
            let res = await iterator.next();

            if (res.value && res.value.value.toString()) {
                let jsonRes = {};
                console.log(res.value.value.toString('utf8'));

                if (isHistory && isHistory === true) {
                    jsonRes.TxId = res.value.tx_id;
                    jsonRes.Timestamp = res.value.timestamp;
                    jsonRes.IsDelete = res.value.is_delete.toString();
                    try {
                        jsonRes.Value = JSON.parse(res.value.value.toString('utf8'));
                    } catch (err) {
                        console.log(err);
                        jsonRes.Value = res.value.value.toString('utf8');
                    }
                } else {
                    jsonRes.Key = res.value.key;
                    try {
                        jsonRes.Record = JSON.parse(res.value.value.toString('utf8'));
                    } catch (err) {
                        console.log(err);
                        jsonRes.Record = res.value.value.toString('utf8');
                    }
                }
                allResults.push(jsonRes);
            }
            if (res.done) {
                console.log('end of data');
                await iterator.close();
                console.info(allResults);
                return allResults;
            }
        }
    }

    async revokeOrgAccess(stub, args) {
        console.info('============= START : revokeOrgAccess ===========');
        if (args.length != 2) {
            throw new Error('Incorrect number of arguments. Expecting 2');
        }

        let getOrg = await stub.getState(args[0]);
        let orgResult = JSON.parse(getOrg);
        orgResult.isRevoked = args[1];

        await stub.putState(args[0], Buffer.from(JSON.stringify(orgResult)));
        console.info('============= END : revokeOrgAccess ===========');
    }


};

shim.start(new Chaincode());
